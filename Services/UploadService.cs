﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace clearskylogic.Services
{
    public class UploadService
    {
        [HttpPost]
        public string UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return "file not selected";

            var path = Path.Combine("wwwroot",
                        file.FileName);

            var fileName = Path.Combine(
            file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return fileName;
        }

    }
}
