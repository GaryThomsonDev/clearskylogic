﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using clearskylogic.Data;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;
using System.Net.Mail;

namespace clearskylogicproperties1.Controllers
{
    public class GetPropertiesController : Controller
    {
        //PropertiesController propertiesController;
        private readonly clearskylogicContext _context;

        public GetPropertiesController(clearskylogicContext context)
        {
            _context = context;
        }

        //
        // GET: /GetPropertiesController
        public async Task<IActionResult> Index()
        {
            return View(await _context.Property.ToListAsync());
        }

        // GET: PropertyList/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Property
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@property == null)
            {
                return NotFound();
            }

            return View(@property);
        }

        //
        // GET: /GetProperties/SendEnquiry
        public IActionResult SendEnquiry()
        {
            return View();
        }
    }
}
