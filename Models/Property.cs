﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clearskylogic.Models
{
    public class Property
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public decimal price { get; set; }
        public string Bedroom { get; set; }
        public string Image { get; set; }
    }
}
