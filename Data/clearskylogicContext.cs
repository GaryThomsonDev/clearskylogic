﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using clearskylogic.Models;

namespace clearskylogic.Data
{
    public class clearskylogicContext : DbContext
    {
        public clearskylogicContext (DbContextOptions<clearskylogicContext> options)
            : base(options)
        {
        }

        public DbSet<clearskylogic.Models.Property> Property { get; set; }
    }
}
